const   nodemailer = require("nodemailer");
require("dotenv").config();

module.exports = nodemailer.createTransport({
    host:process.env.smtp_host,
    port:process.env.hasOwnProperty("smtp_port") ? parseInt(process.env.smtp_port) : 465,
    secure:process.env.hasOwnProperty("smtp_secure") ? (parseInt(process.env.smtp_secure) ? true : false) : true,
    auth: {
        user: process.env.smtp_user,
        pass: process.env.smtp_pass
    }
});
