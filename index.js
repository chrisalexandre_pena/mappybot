require("dotenv").config();
const   mappy       =   require("./src/Mappy"),
        companies   =   require("./base_files/Companies"),
        CCIAdresses =   require("./base_files/CCIAdresses"),
        fs          =   require("fs"),
        transporter = require("./src/ErrorReports/Transporter");

(async function(){
    let i=1;
    fs.writeFileSync("result.json","[]",err=>{if(err)console.log(err);});
    fs.writeFileSync("errors.json","[]",err=>{if(err)console.log(err);});
    for (let company of companies) {
        let result = fs.existsSync("result.json") ? JSON.parse(fs.readFileSync("result.json","UTF-8")) : [],
            errors = fs.existsSync("errors.json") ? JSON.parse(fs.readFileSync("errors.json","UTF-8")) : [];
        try {
            console.clear();
            console.log(`${company["Raison sociale"].trim()} (depuis ${company["CCI Référente"]})`);
            console.log(`Avancée: ${Math.trunc(100*i/companies.length)}%`);
            console.log(`Succès: ${result.length}(${errors.length+result.length ? Math.trunc(100*result.length/(errors.length+result.length)) : 0}%)`);
            console.log(`Echecs: ${errors.length}(${errors.length+result.length ? Math.trunc(100*errors.length/(errors.length+result.length)) : 0}%)`);
            company.mappy = await mappy({
                from:   CCIAdresses[company["CCI Référente"]],
                to:     `${company["Adresse postale"].trim()}, ${company.CP} ${company.Ville.trim()}`
            });
            result.push(company);
            fs.writeFileSync(
                "result.json",
                JSON.stringify(result),
                err=>{if(err)console.log(err);}
            );
        }catch(err) {
            errors.push({
                "Raison sociale":   company["Raison sociale"],
                "Siren":            company.Siren
            });
            fs.writeFileSync(
                "errors.json",
                JSON.stringify(errors),
                err=>{if(err)console.log(err);}
            );
        }
        i++;
    }
    console.log("finished!");
    let mailOptions = {
            from:           '"MappyBot" <chrisalexandre.pena@gmail.com>',
            to:             "chrisalexandre.pena@gmail.com",
            subject:        "MappyBot a fini de tourner",
            attachments:    [
                {
                    filename:   "result.json",
                    path:       "./result.json"
                },
                {
                    filename:   "errors.json",
                    path:       "./errors.json"
                }
            ]
        };
        transporter.sendMail(mailOptions,(error,info)=>{
            if (error) return console.log(error);
            console.log('Message sent: %s', info.messageId);
        });
})();

// mappy({
//     from:"Blois",
//     to:"Belle-Isle-en-Terre"
// }).then(console.log);
