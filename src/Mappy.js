const   Nightmare   =   require("nightmare");

module.exports = async function({from,to}){
    let nm = new Nightmare(/*{ show: true }*/),
        data = await nm
            .goto("https://fr.mappy.com/itineraire")
            .wait(()=>document.querySelectorAll('.MultipathTabStepsView-stepList input').length === 2)
            .insert('.MultipathTabStepsView-stepList input[placeholder="Départ"]',from)
            .insert('.MultipathTabStepsView-stepList input[placeholder="Arrivée"]',to)
            .click('ul.transportModes-iscroll-slider li[data-transport-id="voiture"]')
            .click("a.DateView-toggle-link")
            .select("#hours","23")
            .select("#minutes","55")
            .click('form.MultipathTabView-form button[type="submit"')
            .wait(()=>{
                return Array.from(document.querySelectorAll('.js-routes ul.MultipathTabRoutesView-routeList li.MultipathTabRoutesView-routeList-item'))
                    .filter(e=>e.hasAttribute("data-route-index") && e.hasAttribute("data-mode") && e.getAttribute("data-mode") === "voiture")
                    .length >= 1;
            })
            .evaluate(()=>{
                return Array.from(document.querySelectorAll('.js-routes ul.MultipathTabRoutesView-routeList li.MultipathTabRoutesView-routeList-item'))
                    .filter(e=>e.hasAttribute("data-route-index") && e.hasAttribute("data-mode") && e.getAttribute("data-mode") === "voiture")
                    .map(e=>{
                        let nodes = [].concat(
                            Array.from(e.querySelectorAll(".MultipathTabRoutesView-routeList-item-content-details .MultipathTabRoutesView-routeList-item-content-details-summary > p")),
                            Array.from(e.querySelectorAll(".MultipathTabRoutesView-routeList-item-content-details .MultipathTabRoutesView-routeList-item-content-numbers > p"))
                        ),
                        data = {};
                        for (let node of nodes) {
                            let label = node.className.split("-").pop();
                            if (!data.hasOwnProperty(label)) data[label] = node.textContent;
                            else data[label] += `\n${node.textContent}`;
                        }
                        return data;
                    })
            })
            .end();

    return data.map(e=>{
        let toll = /\ndont\s*([\d,]+)\s*€\s*de\s*péage/gi.test(e.label) ? parseFloat(/\ndont\s*([\d,]+)\s*€\s*de\s*péage/gi.exec(e.label)[1].replace(",",".")) : 0,
            delay = /actuellement\s*(.+?)\s*de\s*retard\s*trafic/gi.test(e.label) ? /actuellement\s*(.+?)\s*de\s*retard\s*trafic/gi.exec(e.label)[1] : null;
        return {
            title:e.title,
            duration:{
                hours: /(\d+)\s*h\s*(\d+)/gi.test(e.duration) ? parseInt(/(\d+)\s*h\s*(\d+)/gi.exec(e.duration)[1]) : 0,
                minutes: /(\d+)\s*h\s*(\d+)/gi.test(e.duration) ? parseInt(/(\d+)\s*h\s*(\d+)/gi.exec(e.duration)[2]) : parseInt(/(\d+)\s*min/gi.exec(e.duration)[1])
            },
            delay: delay,
            toll: toll,
            fuel: parseFloat(/([\d,]+)\s*€/gi.exec(e.pricing)[1].replace(",","."))-toll
        };
    });
};
